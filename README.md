# ESAP E2E

End to end testing of ESAP examples.


## Installation

The E2E testing uses the pytest framework. The install the dependencies, preferably in a virtual environment:
```bash
pip install playwright pytest-playwright pytest-xdist
playwright install
```

## Writing end-to-end tests

Playwright can record a sequence of actions by a user interacting with a web browser.
To start the recording:
```bash
playwright codegen https://sdc-dev.astron.nl/esap-gui/
```
The generated script can then be the basis of an end-to-end test.


## Running the end-to-end test suite

To run all the tests on the ESAP development server hosted at ASTRON, simply type:
```bash
pytest
```

To run a specific test:
```bash
pytest tests/test_front_page.py
```

The option `--base-url` can be used to specify a server other than the default ESAP development server.

> _Note_ Because of this [issue](https://github.com/pytest-dev/pytest-xdist/issues/800), it is required to specify the base URL to run the tests in parallel:
> ```shell
> pytest --base-url https://sdc-dev.astron.nl/esap-gui/ --numprocesses auto
> ```

It is possible to see the slow-motion replay of the user interactions by selecting a browser:

```bash
pytest --slowmo 1000 --headed --browser firefox tests/test_mybinder_apertif_vo.py[firefox-ASTRON VO Apertif]
```


## Gitlab CI

The end-to-end tests are executed by a GitLab CI job, which uses a Docker image built for this purpose. Within this image, the project git repository is cloned and the tests are performed with the command:
```shell
pytest --base-url $CI_ENVIRONMENT_URL --numprocesses auto
```
where `$CI_ENVIRONMENT_URL` corresponds to the URL of the ESAP server under test.
