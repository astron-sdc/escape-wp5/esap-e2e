FROM mcr.microsoft.com/playwright:v1.23.0-focal

RUN apt-get update && apt-get install --yes python3 python3-pip
RUN pip install --upgrade pip
RUN pip install \
  --no-cache-dir \
  --root-user-action=ignore \
  pytest playwright pytest-playwright pytest-xdist
RUN playwright install
