from playwright.sync_api import Page


def test_front_page_is_working(page: Page) -> None:
    page.goto('')
    assert page.title() == 'ESAP-GUI'
