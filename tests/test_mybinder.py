import pytest

from playwright.sync_api import Page, TimeoutError

from .helpers import assert_no_python_error, write_screenshot, write_notebook_screenshot

NOTEBOOK_READY_TIMEOUT = 5 * 60  # seconds


@pytest.mark.parametrize(
    'workflow',
    [
        'agnpy',
        'Apertif Visualization',
        'ASTRON VO Apertif',
        'CDS MOCPy',
        'CSIC-IAA HCG-16 workflow',
        'CTA gammapy Example',
        'Dark matter constraints from dwarf galaxies: a data-driven LAT analysis',
        'eossr',
        'ESCAPE template project',
        'TOPCAT',
        'Zooniverse Muon Hunters',
        'Zooniverse: Advanced Aggregation with Caesar',
        'Zooniverse: Advanced Project Building',
        'Zooniverse: Integrating Machine Learning',
    ]
)
def test_workflow(page: Page, workflow: str) -> None:
    page.goto('interactive')

    # Select workflow
    try:
        page.locator('[placeholder="Search for Workflows"]').click()
        page.locator('[placeholder="Search for Workflows"]').fill(workflow)
        page.locator(f'text={workflow}Description: >> label span').click()
        page.locator("text=Next").click()
    except TimeoutError:
        write_screenshot(page, workflow)
        raise

    # Select Compute Facility
    page.locator("text=MyBinderDescription >> label span").click()
    with page.expect_popup() as popup_info:
        page.locator('a:has-text("Deploy")').click()
    page1 = popup_info.value

    # Binder deployment
    try:
        assert page1.title() == 'Binder'
        page1.locator('button[title^="Restart the kernel, then"]').click(
            timeout=NOTEBOOK_READY_TIMEOUT * 1000
        )
        page1.locator('button:has-text("Restart")').click()
    except (AssertionError, TimeoutError):
        write_screenshot(page1, workflow)
        raise

    try:
        assert_no_python_error(page1)
        write_notebook_screenshot(page1, workflow, False)
    except AssertionError:
        write_notebook_screenshot(page1, workflow)
        raise
