import time
from pathlib import Path

from playwright.sync_api import Page

__all__ = [
    'assert_no_python_error',
    'write_screenshot',
    'write_notebook_screenshot',
]

def assert_no_python_error(page: Page) -> None:
    """Asserts that no error has been detected in the notebook."""
    wait_until_completion(page)
    if error_detected(page):
        raise AssertionError('Notebook raised an error.')


def wait_until_completion(page: Page) -> None:
    """Waits until the test finishes."""
    while is_running(page):
        time.sleep(1)


def is_running(page: Page) -> bool:
    """Returns true if an input cell is preceded by `[*]:`."""
    return page.locator('div.jp-InputArea-prompt:text("[*]:")').count() > 0


def error_detected(page: Page) -> bool:
    """Returns true if an error has been detected in the notebook."""
    error_locator = page.locator(
        'div[data-mime-type="application/vnd.jupyter.stderr"]:has-text("'
        '---------------------------------------------------------------------------")')
    return error_locator.count() > 0


def write_screenshot(page: Page, name: str, failure: bool = True) -> None:
    path = get_screenshot_path(name, failure)
    page.screenshot(path=path, full_page=True)


def write_notebook_screenshot(page: Page, name: str, failure: bool = True) -> None:
    """Writes screenshot of the notebook to disk."""
    path = get_screenshot_path(name, failure)
    notebook_screenshot(page, path)


def get_screenshot_path(name: str, failure: bool) -> Path:
    """Returns the path of the screenshot file."""
    name = name.replace(' ', '_').replace(':', '').lower()
    name = f'{name}.png'
    base_path = Path(__file__).parents[1]
    successful_base_path = base_path / 'successful_screenshots'
    successful_base_path.mkdir(exist_ok=True)
    failed_base_path = base_path / 'failed_screenshots'
    failed_base_path.mkdir(exist_ok=True)

    if failure:
        successful_base_path.joinpath(name).unlink(missing_ok=True)
        path = failed_base_path / name
    else:
        failed_base_path.joinpath(name).unlink(missing_ok=True)
        path = successful_base_path / name

    return path


def notebook_screenshot(page: Page, path: Path) -> None:
    """Takes a screenshot of the notebook and writes it to disk."""
    notebook = page.locator('div.jp-NotebookPanel-notebook')
    assert notebook.count() == 1

    original_size = page.viewport_size
    if not original_size:
        raise Exception('No viewport size.')

    hidden_height: int = page.evaluate("""() =>
    {
        const notebook = document.getElementsByClassName("jp-NotebookPanel-notebook")[0]
        return notebook.scrollHeight - notebook.clientHeight
    }"""
    )
    full_size = {
        'width': original_size['width'],
        'height': original_size['height'] + hidden_height,
    }
    page.set_viewport_size(full_size)
    notebook.screenshot(path=path)
    page.set_viewport_size(original_size)
